'''To plot no .of registration per district'''
import csv
import matplotlib.pyplot as plt

district_pin = {}
district_reg_2015 = {}


def dist_pin_mp():
    '''creates a map of district and pincodes'''
    with open('district.csv', 'r', encoding='iso-8859-1') as csv_file:
        file = csv.DictReader(csv_file)
        for row in file:
            district_pin[row['Pin Code']] = row['District']


def calc_dist_regist():
    '''calculates the count of registrations for every year'''
    with open('Maharashtra.csv', 'r', encoding='iso-8859-1')as csv_file:
        file = csv.DictReader(csv_file)
        for row in file:
            yearstr = row['DATE_OF_REGISTRATION']
            yearL = yearstr.split('-')
            year = yearL[-1]
            addressstr = row['REGISTERED_OFFICE_ADDRESS']
            addressL = addressstr.split(' ')
            pin_code = addressL[-1]
            # print(type(pin_code))
            if pin_code in district_pin and year == '2015':
                if district_pin[pin_code] not in district_reg_2015:
                    district_reg_2015[district_pin[pin_code]] = 1
                else:
                    district_reg_2015[district_pin[pin_code]] += 1
    # print(district_reg_2015)


def plot_dis_year_regis():
    '''plots district vs year registrations'''
    plt.figure(figsize=(12, 6))
    plt.title('Company Registrations in various Districts in 2015')
    plt.bar(list(district_reg_2015.keys()), list(district_reg_2015.values()))
    plt.xticks(rotation=90)
    plt.xlabel("Districts")
    plt.xlabel("No. of company registrations in 2015")
    plt.tight_layout()
    plt.show()


dist_pin_mp()
calc_dist_regist()
plot_dis_year_regis()
