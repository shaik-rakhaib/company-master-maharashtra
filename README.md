# Company Master - Maharashtra

## Installation


You can download the Company Master - Maharashtra csv files from [here](https://data.gov.in/catalog/company-master-data) and  can find districts data from [here](https://www.goldenchennai.com/pin-code/maharashtra-postal-code/)

Create and activate a virtual environment (optional but recommended):

```bash
python3 -m venv my_env 
```
To activate the virtual environment, run this command
```bash
my_env/bin/activate
```
If everything has gone well without any issues, your shell prompt(python in this case) should start with the name of your virtual environment and should look something like this.
  ```bash
  (my_env) shaik-rakhaib@shaik-aspire:~
  ```
* In my case , 'my_env' is virtual environment that I activated.

All the required libraries have been included in the requirements.txt file.

To install them, you can use the following command,
```bash
pip install -r requirements.txt
```

Make sure to have all the files in the current directory before running python files.
