'''
    Plots a histogram with respect to certain bins
'''
import csv
import matplotlib.pyplot as plt
authorized_capitals = []
bin_edges = [0, 100000, 10000000, 100000000, 1000000000, float('inf')]

def calc_histogram():
    '''transforms csv file to required data structure'''
    with open('Maharashtra.csv','r',encoding='iso-8859-1') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            authorized_cap = float(row['AUTHORIZED_CAP'])
            authorized_capitals.append(authorized_cap)

def plot_histogram():
    '''plots the histogram'''
    calc_histogram()
    hist = [0] * (len(bin_edges) - 1)
    for cap in authorized_capitals:
        for i in range(len(bin_edges) - 1):
            if bin_edges[i] <= cap < bin_edges[i + 1]:
                hist[i] += 1
                break
    labels = ['<= 1L', '1L to 10L', '10L to 1Cr', '1Cr to 10Cr', '> 10Cr']
    plt.bar(labels, hist, width=1, align='center')
    plt.xlabel('Authorized Capital Intervals')
    plt.ylabel('Number of Companies')
    plt.title('Histogram of Authorized Capital')
    plt.tight_layout()
    plt.show()

plot_histogram()
