import csv
import matplotlib.pyplot as plt
from collections import defaultdict

regist_year_acti = defaultdict(lambda: defaultdict(int))

# Read the CSV file with ISO-8859-1 encoding
with open('Maharashtra.csv', 'r', encoding='iso-8859-1') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        registration_date = row['DATE_OF_REGISTRATION']
        if len(registration_date) == 8:
            registration_year = registration_date[-2:]
        elif len(registration_date) == 10:
            registration_year = registration_date[-2:]
        else:
            continue
        busi_acti = row['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']

        # Update the registration count for the specific year and activity
        regist_year_acti[registration_year][busi_acti] += 1

# Sort the years in descending order (most recent first)
sorted_years = sorted(regist_year_acti.keys(), reverse=True)

# Select the last 10 years
last_10_years = sorted_years[:10]

# Initialize a dictionary to store the top 5 activities for each year
top_5_activities_by_year = defaultdict(list)

# Find the top 5 activities for each of the last 10 years
for year in last_10_years:
    year_data = regist_year_acti[year]
    sorted_activities = sorted(year_data, key=year_data.get, reverse=True)
    top_5_activities_by_year[year] = sorted_activities[:5]

# Create a grouped bar plot for the top 5 activities by year
plt.figure(figsize=(12, 6))

# Define a set of colors to use in the plot
colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'orange', 'brown']
print(len(last_10_years))
# Ensure that the number of colors matches the number of years
if len(colors) < len(last_10_years):
    raise ValueError("Please provide enough colors for the years")

for i, year in enumerate(last_10_years):
    activity_counts = [regist_year_acti[year][activity]
                       for activity in top_5_activities_by_year[year]]
    x = range(len(top_5_activities_by_year[year]))
    plt.bar([pos + i * 0.15 for pos in x],
            activity_counts, width=0.15, label=year, color=colors[i])

plt.xlabel('Principal Business Activity')
plt.ylabel('Number of Registrations')
plt.title('Top 5 Principal Business Activities for the Last 10 Years')
plt.xticks([pos + 0.3 for pos in x],
           top_5_activities_by_year[last_10_years[0]])

# Add a legend to the plot
plt.legend(title='Year', loc='upper right')

# Rotate x-axis labels for better visibility
plt.xticks(rotation=45)

# Display the plot
plt.tight_layout()
plt.show()
