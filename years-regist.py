'''
    To count no. of registrations per year
'''
import csv
import matplotlib.pyplot as plt

year_company = {}


def calc_regis_year():
    with open('Maharashtra.csv', 'r', encoding='iso-8859-1') as file:
        company_data = csv.DictReader(file)
        for row in company_data:
            date = row['DATE_OF_REGISTRATION']
            yearL = date.split('-')
            try:
                year = int(yearL[-1])
                if year < 2019:
                    if year not in year_company:
                        year_company[year] = 1
                    else:
                        year_company[year] += 1
            except:
                continue


def plot_regis_year(years, count):
    plt.figure(figsize=(18, 10))
    plt.bar(years, count, align='center', alpha=0.7)
    plt.xlabel('Registration Year')
    plt.ylabel('Number of Registrations')
    plt.title('Company Registrations by Year')
    # for i in range(len(years)):
    #     plt.text(years[i],count[i],count[i],ha='center',va='bottom')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()


def execute():
    calc_regis_year()
    years = list(year_company.keys())
    count = list(year_company.values())
    plot_regis_year(years, count)


execute()
